import { z } from 'zod';

//https://www.keycloak.org/docs-api/11.0/rest-api/index.html#_federatedidentityrepresentation
export const FederatedIdentityRepresentationSchema = z.object({
  identityProvider: z.string().optional(),
  userId: z.string().optional(),
  userName: z.string().optional(),
});

export type FederatedIdentityRepresentation = z.infer<typeof FederatedIdentityRepresentationSchema>;

//* extracted from 'IdentityProviderRepresentation' from '@keycloak/keycloak-admin-client/lib/defs/identityProviderRepresentation';
export const IdentityProviderRepresentationSchema = z.object({
  addReadTokenRoleOnCreate: z.boolean().optional(),
  alias: z.string().optional(),
  config: z.record(z.string(), z.any()).optional(),
  displayName: z.string().optional(),
  enabled: z.boolean().optional(),
  firstBrokerLoginFlowAlias: z.string().optional(),
  hasStorage: z.boolean().optional(),
  internalId: z.string().optional(),
  linkOnly: z.boolean().optional(),
  postBrokerLoginFlowAlias: z.string().optional(),
  providerId: z.string().optional(),
  storeToken: z.boolean().optional(),
  trustEmail: z.boolean().optional(),
});

export type IdentityProviderRepresentation = z.infer<typeof IdentityProviderRepresentationSchema>;
