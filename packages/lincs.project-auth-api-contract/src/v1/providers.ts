import { initContract } from '@ts-rest/core';
import { z } from 'zod';
import { IdentityProviderRepresentationSchema } from './types';

const c = initContract();

export const providersContract = c.router(
  {
    getAll: {
      method: 'GET',
      path: `/`,
      headers: z
        .object({
          authorization: z.string().optional(),
        })
        .optional(),
      responses: {
        200: z.array(IdentityProviderRepresentationSchema),
        500: c.type<{ message: string }>(),
      },
      summary: 'Get identity providers',
    },
  },
  {
    pathPrefix: '/providers',
  },
);
