import { FederatedIdentityRepresentationSchema } from './types';
import { initContract } from '@ts-rest/core';
import { z } from 'zod';

const c = initContract();

export const usersContract = c.router(
  {
    getLinkedAccounts: {
      method: 'GET',
      path: `/:username/linked-accounts`,
      headers: z.object({
        authorization: z.string(),
      }),
      responses: {
        200: z.array(FederatedIdentityRepresentationSchema),
        401: c.type<{ message: string }>(),
        404: c.type<{ message: string }>(),
        500: c.type<{ message: string }>(),
      },
      summary: 'Get user linked accounts',
    },
    getLinkAccountUrl: {
      method: 'GET',
      path: `/:username/link-account-url`,
      headers: z.object({
        authorization: z.string(),
      }),
      query: z.object({
        /**
         * @type {string} - The indentity provider to link.
         */
        provider: z.string(),
        /**
         * @type {string} - The URL to redirect to after linking.
         * Usually the URL from where the call was originated.
         * Or the website root.
         */
        redirectUri: z.string(),
      }),
      responses: {
        200: z.object({ url: z.string() }),
        401: c.type<{ message: string }>(),
        404: c.type<{ message: string }>(),
        500: c.type<{ message: string }>(),
      },
      summary: 'Get url to link the provider to the user account',
    },
  },
  {
    pathPrefix: '/users',
  },
);
