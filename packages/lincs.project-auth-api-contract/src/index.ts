import { initContract } from '@ts-rest/core';
import { providersContract, usersContract } from './v1';

export * from './v1/types';

const c = initContract();

export const contract = c.router({
  v1: c.router(
    {
      providers: providersContract,
      users: usersContract,
    },
    {
      pathPrefix: '/v1',
      strictStatusCodes: true,
    },
  ),
});
