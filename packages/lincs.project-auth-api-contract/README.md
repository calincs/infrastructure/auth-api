# LINCS Auth API Contract

Defines the contract for the [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api). It uses [TS-REST](https://ts-rest.com/) in combination with [Zod](https://zod.dev/) to determine the request and response objects. These objects can be used to validate calls to each endpoint.

It can be used to build a JavaScript helper to access the endpoints of the [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api) using a standardized fetcher. It allows for better endpoint discoverability and fully typed request parameters and responses. The API contract is also available to create a custom fetcher.

It must be used in conjunction with [`@ts-rest/core`](https://www.npmjs.com/package/@ts-rest/core)

## How to use the client helper

### Install

```text
npm install @lincs.project/auth-api-contract @ts-rest/core
```

### Create the client Adapter and use it

```js
import { contract } from '@lincs.project/auth-api-contract';
import { initClient } from '@ts-rest/core';

const lincsAuthApiAdapter = initClient(contract, {
  baseUrl: 'https://auth-api.dev.lincsproject.ca',
  baseHeaders: {}
});

const response = await lincsAuthApiAdapter.v1.providers.getAll();

if (response === 200) {
  console.log(response.body)
  /*
    [{
      "providerId": "gitlab",
      "enabled": true,
      "linkOnly": false,
      "config": {...},
      ...
    }]
  */

```

## Endpoints

This list may not reflect the current state of the API. Please check the open api documentation here: [http://auth-api.lincsproject.ca](http://auth-api.lincsproject.ca).

### GET /v1/providers

#### Response

Return a list of available external identity providers.

```JSON
[
  {
    "providerId": "orcid",
    "enabled": true,
    "linkOnly": true,
    "config": {...},
    ...
  },
  {
    "providerId": "gitlab",
    "enabled": true,
    "linkOnly": false,
    "config": {...},
    ...
  }
]
```

#### Using the adapter

```js
const providers = await lincsAuthApiAdapter.v1.providers.getAll();
```

### GET /v1/users/{username}/linked-accounts

#### Request

##### Headers

`Authorization: Bearer {access_token}` - User's Keycloak access token

##### Params

`username: string` - the username

#### Response

Return a list of external identity providers connected to a specific user.

```JSON
[
  {
    "identityProvider": "orcid",
    "userId": {userId},
    "userName": {userName},
  },
  {
    "identityProvider": "github",
    "userId": {userId},
    "userName": {userName},
  }
]
```

#### Using the adapter

```js
const accounts = await lincsAuthApiAdapter.v1.users.getLinkedAccounts({
  header: { autorization: 'Bearer {access_token}' },
  params: { username },
});
```

### GET /v1/users/{username}/link-account-url

Generate a URL to request Keycloak to connect a specific provider to a specified user.

#### Request

##### Headers

`Authorization: Bearer {access_token}` - User's Keycloak access token

##### Params

`username: string` - the username

##### Query

`provider: string` - the provider to connect
`redirectUri: string` - where to redirect the browser after connecting the account.

#### Response

An object with the `url` property.

```JSON
{
  "url": "https://keycloak.lincsproject.ca/...",
}
```

#### Using the adapter

```js
const accounts = await lincsAuthApiAdapter.v1.users.getLinkAccountUrl({
  header: { autorization: 'Bearer {access_token}' },
  params: { username },
  query: {
    provider: { providerId },
    redirectUri: { url },
  },
});
```

## Development

Any changes in this package will reflect on [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api).

### Versioning

Since this is a contract for a web service API, it is important to strategically think about changes as they might break other services that depend on this API. We started with version `v1`.

A further development that adds, removes, or changes the endpoints should be allocated in a new version. However, keep the previous version available to allow other services to upgrade their codebase.

### Linter

**Attention!**

This project uses and deploys ESM. We have configured a tight linter with strict Typescript and format (prettier) rules. Some of these rules can be relaxed if needed. Check eslint config: `.eslintrc.cjs`.

The linter always runs before any commit. But you can also run it at any time to check the code.

```node
npm run lint
```

### Build

```node
npm run build
```

or in development mode

```node
npm run dev
```
