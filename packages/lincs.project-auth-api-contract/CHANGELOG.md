# @lincs.project/auth-api-contract

## 1.2.1

### Patch Changes

- Bump up version

## 1.2.0

### Minor Changes

- [eacd64d3d31130e275a13c4aa8f5fd93ea971a17] IdentityProviderRepresentation: add attribute `hasStorage`

### Patch Changes

- [1f541a91ac27e00782f7fd3c4ad346cd1e3340f4] export types
- [f7751d5422598306d3345a1040d71cb4e842b564] update dependencies:
  - core:
    - bump:
    - @ts-rest/core@3.45.2
    - zod@3.23.8
  - dev: update: eslint@8.57.0

## 1.1.0

### Minor Changes

- [7cc440cadee13da715c39049e3378e1fe37c966d] return strict Status Codes (#8)

### Patch Changes

- [2be7c15f0638c9f80cb9835c61583d622783de38] tweak tsconfig. Make it a module
- [574a9f35cb2be226e4df59011a1442bbfe049b92] update dependencies
  - core:
    - update:
      - @ts-rest/core@3.45.0
      - zod@2.23.0
  - dev:
    - upgrade: tsup@8.0.2
    - update: typescript@5.4.5

## 1.0.3

### Patch Changes

- [8a6a8297ac569dcf0c22d111846cddc88ca62378] update dependencies
  - core:
    - update:
      - @ts-rest/core@3.30.4
      - zod@3.22.4
  - dev:
    - update:
      - eslint@8.53.0
      - typescript@5.2.2
  - peer: update: @ts-rest/core@3.30.4

## 1.0.2

### Patch Changes

- Docs
  - Formating [ba857df04ed866484fd4ac90e3576c25c3ea4d2b]
- Update dependencies [5c3aee53f9a26ddd520461a7cdd756caf050fff9]:
  - core: update: @ts-rest/core@3.27.0
  - peer: update: @ts-rest/core@3.27.0

## 1.0.1

### Patch Changes

Rename endpoints functions.

- v1.providers.providers => v1.providers.getAll
- v1.users.linkedAccounts => v1.users.getLinkedAccounts
- v1.users.linkAccountUrl => v1.users.getLinkAccountUrl

**Attention**: This is not a breaking change for the API. But it **breaks the adapter**. However, since we are not publicly launched yet, this is a **patch change**.
