FROM node:20-alpine

RUN npm install husky turbo tsup -g

WORKDIR /app
COPY . .
RUN npm install

RUN npm run build

EXPOSE 3000
CMD [ "npm", "run", "start" ]