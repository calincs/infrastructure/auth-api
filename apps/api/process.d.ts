declare namespace NodeJS {
  export interface ProcessEnv {
    ADMIN_USER: string;
    ADMIN_PASSWORD: string;
    AUTH_API_ENV: string;
    KEYCLOAK_BASE_URL: string;
    SESSION_SECRET: string;
  }
}
