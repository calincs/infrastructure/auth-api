# LINCS Auth API

This service exposes Keycloak functionality to authorized LINCS applications.

We use [Express](https://expressjs.com/) and [TS-REST](https://ts-rest.com/) as the backbone to build the server, combined with [@keycloak/keycloak-admin-client](https://www.npmjs.com/package/@keycloak/keycloak-admin-client) and
[keycloak-connect](https://github.com/keycloak/keycloak-nodejs-connect) to access, control, and protect the Keycloak instance.

The contract for this API is defined in the [@lincs.project/auth-api-contract](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/packages/lincs.project-auth-api-contract).

The endpoints require an authorization token with access to the _admin_ role of the _admin-cli_ client in Keycloak. This token can be associated with a service account or a regular user account.

The requests are validated in real-time using [Zod](https://zod.dev/). The server will respond immediately with the most appropriate status code and error message if the parameters, queries, or body post property are invalid for a specific endpoint.

The API contract and interactive documentation for the LINCS Auth API are available here: [http://auth-api.lincsproject.ca](http://auth-api.lincsproject.ca). You can test out API calls from within the documentation itself. You can also download the Open Api file here: [http://auth-api.lincsproject.ca/open-api.json](http://auth-api.lincsproject.ca/open-api.json).

## Auth-Api Client

The [auth-api-contract](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/packages/lincs.project-auth-api-contract)
can be used to build a JavaScript helper to access the endpoints of the [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api) using a standardized fetcher. It allows for better endpoint discoverability and fully typed request parameters and responses. The API contract is also available to create a custom fetcher.

## Endpoints

This list may not reflect the current state of the API. Please check the documentation here: [http://auth-api.lincsproject.ca](http://auth-api.lincsproject.ca).

### GET /v1/providers

Return a list of available external identity providers.

```bash
curl --location --request GET 'http://auth-api.lincsproject.ca/v1/providers' \
```

Response

```JSON
[
  {
    "providerId": "orcid",
    "enabled": true,
    "linkOnly": true,
    "config": {...},
    ...
  },
  {
    "providerId": "gitlab",
    "enabled": true,
    "linkOnly": false,
    "config": {...},
    ...
  }
]
```

### GET /v1/users/{username}/linked-accounts

Return a list of external identity providers connected to a specific user.

```bash
curl --location --request GET 'https://auth-api.lincsproject.ca/v1/users/{username}/linked-accounts' \
--header 'Authorization: Bearer eyJhbG...'
```

#### Request

##### Headers

`Authorization: Bearer {access_token}` - User's Keycloak access token

##### Params

`username: string` - the username

#### Response

```JSON
[
  {
    "identityProvider": "orcid",
    "userId": {userId},
    "userName": {userName},
  },
  {
    "identityProvider": "github",
    "userId": {userId},
    "userName": {userName},
  }
]
```

### GET /v1/users/{username}/link-account-url

Generate a URL to request Keycloak to connect a specific provider to a specified user.

```bash
curl --location --request GET 'http://auth-api.lincsproject.ca/v1/users/{username}link-account-url?provider=github&redirectUri=https://...' \
--header 'Authorization: Bearer eyJh...'
```

#### Request

##### Headers

`Authorization: Bearer {access_token}` - User's Keycloak access token

##### Params

`username: string` - the username

##### Query

`provider: string` - the provider to connect
`redirectUri: string` - where to redirect the browser after connecting the account.

#### Response

An object with the `url` property.

```JSON
{
  "url": "https://keycloak.lincsproject.ca/...",
}
```

## Development

### Versioning

Since this is a contract for a web service API, it is important to strategically think about changes as they might break other services that depend on this API. We started with version `v1`. See [auth-api-contract](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/packages/lincs.project-auth-api-contract).

A further development that adds, removes, or changes the endpoints should be allocated in a new version. However, keep the previous version available to allow other services to upgrade their codebase.

### Logs

This project uses [Winston](https://github.com/winstonjs/winston) and [Mogan](https://github.com/expressjs/morgan) to print logs to the console.

### Linter

**Attention!**

This project uses and deploys ESM. We have configured a tight linter with strict Typescript and format (prettier) rules. Some of these rules can be relaxed if needed. Check eslint config: `.eslintrc.cjs`.

The linter always runs before any commit. But you can also run it at any time to check the code.

```node
npm run lint
```

### Tests

**Warning: This project has no test yet.**

Some dependencies (e.g., `@keycloak/keycloak-admin-client` and `query-string`) export `ESM` code. ESM is still not widely supported throughout the NODE ecosystem. When combined with Typescript, ESM can be difficult to manage. Even though there are many solutions to run tests using ESM, NODE (v.20) and JEST (v.29) still do not provide full support.

Due to the nature of this project (low-level API that accesses external services that require authentication), it is not trivial to just transpile the code to regular JS.

Future development should check on the state of the testing tool and create tests for this service.

### Run locally

```node
npm install
npm run build
npm start
```

or in development mode

```node
npm run dev
```
