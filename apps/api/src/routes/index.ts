import { contract } from '@lincs.project/auth-api-contract';
import { initServer } from '@ts-rest/express';
import { routerProviders, routerUsers } from './v1';

const s = initServer();

export const router = s.router(contract, {
  v1: {
    users: routerUsers,
    providers: routerProviders,
  },
});
