import { getAdminClient } from '@/src/keycloak/admin';
import createError from 'http-errors';
import { providersWithStorage } from '..';

/**
 * The function `getProvider` retrieves an identity provider based on its alias and returns it, or
 * throws an error if the provider is not found.
 * @param {string} alias - The `alias` parameter is a string that represents the alias of the identity
 * provider you want to retrieve.
 * @returns the identity provider object if it is found, or it is returning an error with a status code
 * of 404 if the identity provider is not found.
 */
export const getProvider = async (alias: string) => {
  const adminClient = await getAdminClient();
  const provider = await adminClient.identityProviders.findOne({ alias });
  if (!provider) return createError(404, 'Identity Provider not found');

  const augmentedIdentityProvider = {
    ...provider,
    hasStorage: provider.providerId ? providersWithStorage.has(provider.providerId) : false,
  };

  return augmentedIdentityProvider;
};
