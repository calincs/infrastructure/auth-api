import { contract } from '@lincs.project/auth-api-contract';
import { initServer } from '@ts-rest/express';
import createError from 'http-errors';
import { getProviders } from './getProviders';

export const providersWithStorage = new Set(['github', 'gitlab']);

const s = initServer();

export const routerProviders = s.router(contract.v1.providers, {
  /* The `providers` function is an asynchronous function that is used as a handler for the
  `/providers` endpoint in the router. */
  getAll: async ({ headers: { authorization } }) => {
    // * The presence of the authorization header defines if providers' properties will be trimmed.
    // ? WARNING: This is not a security check. We are not check the validity of the token. Only its presence.
    const trim = authorization ? false : true;

    const response = await getProviders({ trim });
    if (response instanceof createError.HttpError) {
      return { status: response.status, body: { message: response.message } };
    }

    return { status: 200, body: response };
  },
});
