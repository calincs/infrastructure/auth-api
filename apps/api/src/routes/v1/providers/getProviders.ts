import { getAdminClient } from '@/src/keycloak/admin';
import { getErrorMessage } from '@/src/util/error';
import type IdentityProviderRepresentation from '@keycloak/keycloak-admin-client/lib/defs/identityProviderRepresentation';
import createError from 'http-errors';
import { providersWithStorage } from '.';

/**
 * The function `getProviders` retrieves a list of identity providers using an admin client.
 * @returns The function `getProviders` returns a promise that resolves to the `identityProviders`
 * array if successful. If there is an error, it returns a promise that resolves to an error object
 * with a status code of 500 and an error message.
 */

/**
 * The code is defining a function named `getProviders` that retrieves a list of identity providers
 * using an admin client. The function takes an optional parameter `trim` which is a boolean indicating
 * whether to trim the identity providers or not. If `trim` is `false` or not provided, the function
 * returns the `identityProviders` array as is. If `trim` is `true`, the function creates a new array
 * of trimmed identity providers by mapping over the original array and extracting only the necessary
 * properties (`alias`, `displayName`, `enabled`, `linkOnly`, `providerId`). The function then returns
 * the trimmed identity providers array.
 * @param {trim} boolean - To trim or not to trim
 * @returns {IdentityProviderRepresentation[]} - a list of identity providers.
 */
export const getProviders = async ({ trim }: { trim: boolean } = { trim: false }) => {
  try {
    const adminClient = await getAdminClient();
    const identityProviders = await adminClient.identityProviders.find();

    const augmentedIdentityProviders = identityProviders.map((provider) => ({
      ...provider,
      hasStorage: provider.providerId ? providersWithStorage.has(provider.providerId) : false,
    }));

    if (!trim) return augmentedIdentityProviders;

    const trimmedIdentityProviders = augmentedIdentityProviders.map((provider) => {
      const { alias, displayName, enabled, linkOnly, providerId, hasStorage } = provider;
      const trimmedIdentityProviders: IdentityProviderRepresentation & { hasStorage: boolean } = {
        alias,
        displayName,
        enabled,
        linkOnly,
        providerId,
        hasStorage,
      };
      return trimmedIdentityProviders;
    });

    return trimmedIdentityProviders;
  } catch (error) {
    console.log(error);
    return createError(500, getErrorMessage(error));
  }
};
