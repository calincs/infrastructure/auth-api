import { getAdminClient } from '@/src/keycloak/admin';

/**
 * The function `getUsers` retrieves all users using an admin client.
 * @returns The getUsers function is returning a promise that resolves to the result of calling the
 * find method on the adminClient's users object.
 */
export const getUsers = async () => {
  const adminClient = await getAdminClient();
  return await adminClient.users.find();
};
