import { getAdminClient } from '@/src/keycloak/admin';
import { getErrorMessage } from '@/src/util/error';
import createError from 'http-errors';

/**
 * The function `getLinkedAccounts` retrieves the federated identities linked to a user's account.
 * @param {string} username - The `username` parameter is a string that represents the username of the
 * user whose linked accounts you want to retrieve.
 * @returns The function `getLinkedAccounts` returns a promise that resolves to an array of federated identities.
 */
export const getLinkedAccounts = async (username: string) => {
  try {
    const adminClient = await getAdminClient();

    const users = await adminClient.users.find({ username });
    const user = users[0];
    if (!user?.id) return createError(404, 'User not found');

    const federatedIdentities = await adminClient.users.listFederatedIdentities({
      id: user.id,
    });

    return federatedIdentities;
  } catch (error) {
    console.log(error);
    return createError(500, getErrorMessage(error));
  }
};
