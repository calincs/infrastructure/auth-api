import { getErrorMessage } from '@/src/util/error';
import crypto from 'crypto';
import createError from 'http-errors';
import jsonwebtoken from 'jsonwebtoken';
import queryString from 'query-string';
import { v4 as uuidv4 } from 'uuid';

interface Props {
  authToken: string;
  provider: string;
  redirectUri: string;
}
//* https://www.keycloak.org/docs/latest/server_development/#client-initiated-account-linking

/**
 * The function `getLinkAccountUrl` generates a URL for linking a user account with a specific provider.
 * @param {Props}  - {object}
 * - `authToken`: The authentication token of the user.
 * - `provider`: The provider the user user wants to link.
 * - `redirectUri`: The url to go back to.
 * @returns The function `getLinkAccountUrl` returns the account link URL as a string.
 */
export const getLinkAccountUrl = ({ authToken, provider, redirectUri }: Props) => {
  try {
    const realm = 'lincs';
    const token = jsonwebtoken.decode(authToken.substring(7));
    if (!token || typeof token === 'string') {
      const error = createError(404, 'User token not found');
      return error;
    }

    const userSession = token.session_state as string; // token.getSessionState() or "user session id"
    const clientId = token.azp as string;
    const nonce = uuidv4();
    const input = nonce + userSession + clientId + provider;
    const hash = crypto.createHash('sha256').update(input, 'utf-8').digest().toString('base64url');

    const accountLinkUrl = queryString.stringifyUrl({
      url: `${process.env.KEYCLOAK_BASE_URL}/realms/${realm}/broker/${provider}/link`,
      query: {
        nonce,
        hash,
        client_id: clientId,
        redirect_uri: redirectUri,
      },
    });

    return accountLinkUrl;
  } catch (error) {
    console.log(error);
    return createError(500, getErrorMessage(error));
  }
};
