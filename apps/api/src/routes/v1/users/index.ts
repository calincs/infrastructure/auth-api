import { KC } from '@/src/keycloak/connect';
import { contract } from '@lincs.project/auth-api-contract';
import { initServer } from '@ts-rest/express';
import createError from 'http-errors';
import { getLinkAccountUrl, getLinkedAccounts } from './[user]';

export * from './getUsers';

const s = initServer();

export const routerUsers = s.router(contract.v1.users, {
  /* The `linkedAccounts` property is defining a route handler for the
  `/users/:username/linkedAccounts` endpoint. */
  getLinkedAccounts: {
    middleware: [KC.protect('admin')],
    handler: async ({ headers: { authorization }, params: { username } }) => {
      if (!authorization || Array.isArray(authorization)) {
        return { status: 401, body: { message: 'Unauthorized' } };
      }
      const response = await getLinkedAccounts(username);
      if (response instanceof createError.HttpError) {
        return { status: response.status, body: { message: response.message } };
      }
      return { status: 200, body: response };
    },
  },
  /* The `linkAccountUrl` property is defining a route handler for the `/users/linkAccountUrl`
  endpoint. */
  getLinkAccountUrl: {
    middleware: [KC.protect('admin')],
    handler: async ({ headers: { authorization }, query: { provider, redirectUri } }) => {
      if (!authorization || Array.isArray(authorization)) {
        return { status: 401, body: { message: 'Unauthorized' } };
      }

      const response = getLinkAccountUrl({ authToken: authorization, provider, redirectUri });
      if (response instanceof createError.HttpError) {
        return { status: response.status, body: { message: response.message } };
      }

      return { status: 200, body: { url: response } };
    },
  },
});
