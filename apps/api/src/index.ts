import 'dotenv/config';
import { bgBlue } from 'kleur/colors';
import { createServer } from './server';

const PORT = process.env.PORT ?? 3000;
const server = createServer();
server.listen(PORT, () => console.log(`Listening at ${bgBlue(`http://localhost:${PORT}`)}`));
