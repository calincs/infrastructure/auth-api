import pkg from '@/src/../package.json' assert { type: 'json' };
import { contract } from '@lincs.project/auth-api-contract';
import { generateOpenApi } from '@ts-rest/open-api';

const PORT = process.env.PORT ?? 3000;
const AUTH_API_ENV = process.env.AUTH_API_ENV ?? 'localhost';

const serverUrl =
  AUTH_API_ENV === 'prod'
    ? 'https://auth-api.lincsproject.ca'
    : AUTH_API_ENV === 'stage' || AUTH_API_ENV === 'dev'
      ? `https://auth-api.${AUTH_API_ENV}.lincsproject.ca`
      : `http://localhost:${PORT}`;

export const openApiDocument = generateOpenApi(
  contract.v1,
  {
    info: {
      title: 'LINCS Auth-API',
      description: `Source: [${serverUrl}/open-api.json](${serverUrl}/open-api.json)`,
      version: pkg.version,
    },
    servers: [{ url: serverUrl, description: AUTH_API_ENV }],
    security: [{ bearerAuth: [] }],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    externalDocs: {
      description: 'Project Reposity',
      url: 'https://gitlab.com/calincs/infrastructure/auth-api',
    },
  },
  { setOperationId: true },
);
