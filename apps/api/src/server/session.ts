import session from 'express-session';
import memorystore from 'memorystore';

const MemoryStore = memorystore(session);

export const memoryStore = new MemoryStore({
  checkPeriod: 86400000, // prune expired entries every 24h
});

//*  According to https://www.keycloak.org/docs/latest/securing_apps/#_nodejs_adapter
export const Session = session({
  resave: false,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET ?? 'My Secrect',
  store: memoryStore,
});
