import { KC } from '@/src/keycloak/connect';
import { router } from '@/src/routes';
import { openApiDocument } from '@/src/swagger';
import { contract } from '@lincs.project/auth-api-contract';
import { createExpressEndpoints } from '@ts-rest/express';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import 'dotenv/config';
import express, { type Express } from 'express';
import helmet from 'helmet';
import * as swaggerUi from 'swagger-ui-express';
import { morganWinstonMiddleware } from './logger';
import { Session } from './session';

let server: Express | undefined;

export const createServer = () => {
  if (server) return server;

  server = express();
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(express.json());

  server.use(Session);
  server.use(cors());
  server.use(helmet());
  server.use(compression());
  server.use(morganWinstonMiddleware);

  server.use(KC.middleware({ admin: '/v1' }));
  createExpressEndpoints(contract, router, server);

  server.get('/healthz', (req, res) => res.send('OK'));
  server.get('/open-api.json', (req, res) => res.send(openApiDocument));

  server.use('/', swaggerUi.serve, swaggerUi.setup(openApiDocument));

  return server;
};
