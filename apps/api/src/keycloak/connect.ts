import Keycloak from 'keycloak-connect';
import { memoryStore } from '@/src/server/session';

// According to https://www.keycloak.org/docs/latest/securing_apps/#_nodejs_adapter
const keycloakConfig: Keycloak.KeycloakConfig = {
  realm: 'lincs',
  'auth-server-url': process.env.KEYCLOAK_BASE_URL ?? 'https://keycloak.dev.lincsproject.ca',
  'ssl-required': 'external',
  resource: 'admin-cli',
  'bearer-only': true,
  'confidential-port': 0,
};

export let _keycloak!: Keycloak.Keycloak;

export const initKeycloak = () => {
  if (_keycloak) return _keycloak;

  console.log('Initializing Keycloak...');
  _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
  return _keycloak;
};

export const KC = initKeycloak();
