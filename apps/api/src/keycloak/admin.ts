import KcAdminClient from '@keycloak/keycloak-admin-client';

export const getAdminClient = async () => {
  const kcAdminClient = new KcAdminClient({
    baseUrl: process.env.KEYCLOAK_BASE_URL || 'https://keycloak.dev.lincsproject.ca',
    realmName: 'master',
  });

  // Authorize with username/password
  await kcAdminClient.auth({
    username: process.env.ADMIN_USER ?? 'user',
    password: process.env.ADMIN_PASSWORD,
    grantType: 'password',
    clientId: 'admin-cli',
  });

  // Override client configuration for all further requests
  kcAdminClient.setConfig({ realmName: 'lincs' });

  return kcAdminClient;
};
