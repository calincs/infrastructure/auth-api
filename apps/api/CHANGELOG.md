# auth-api

## 1.2.1

### Patch Changes

- Updated dependencies
  - @lincs.project/auth-api-contract@1.2.1

## 1.2.0

### Minor Changes

- [1fe72c54e9c4f052d9e5904a73bc556421786092] providers: add attrinbute `hasStorage`

### Patch Changes

- Updated dependencies
  - core:
    - update: @lincs.project/auth-api-contract@1.2.0
    - bump:
      - @keycloak/keycloak-admin-client@24.0.4
      - @ts-rest/express@3.45.2
      - @ts-rest/open-api@2.45.2
      - keycloak-connect@24.0.4
  - dev: bump: @types/node@20.12.12

## 1.1.0

### Patch Changes

- [51154fadf71aac011702fc9463f7d9a35a957fe8] Add relative path (tsconfig)
- [c4fe6fee7ca9003ab0b8931ea3b66c18b9c4df54] fix main script path (package.json)
- [fae96556c77a19c8fee97b25d3657bc195961444] update dependencies
  - core:
    - upgrade:
      - @keycloak/keycloak-admin-client@24.0.3
      - keycloak-connect@24.0.3
      - query-string@9.0.0
    - update:
      - @lincs.project/auth-api-contract@1.1.0
      - @ts-rest/express@3.45.0
      - @ts-rest/open-api@3.45.0
      - dotenv@16.4.5
      - express@4.19.2
      - express-session@1.18.0
      - helmet@7.1.0
      - winston@3.13.0
  - dev:
    - upgrade: tsup@8.0.2
    - update:
      - @types/node@20.12.7
      - typescript@5.4.5
    - bump:
      - @types/compression@1.7.5
      - @types/cors@2.8.17
      - @types/express@4.17.21
      - @types/express-session@1.18.0
      - @types/http-errors@2.0.4
      - @types/jsonwebtoken@9.0.6
      - @types/swagger-ui-express@4.1.6
      - @types/uuid@9.0.8

## 1.0.3

### Patch Changes

- [88be8decabd3a62979442772c63ba8b6e19d59af] update dependencies
  - core:
    - update:
      - @ts-rest/express@3.30.4
      - @ts-rest/open-api@3.30.4
      - winston@3.11.0
    - bump:
      - @keycloak/keycloak-admin-client@22.0.5
      - **@lincs.project/auth-api-contract@1.0.3**
      - jsonwebtoken@9.0.2
      - keycloak-connect@22.0.5
      - uuid@9.0.1
  - dev:
    - update:
      - @types/node@20.8.10
      - eslint@8.53.0
      - typescript@5.2.2
    - bump:
      - @types/compression@1.7.4
      - @types/cors@2.8.15
      - @types/express@4.17.20
      - @types/express-session@1.17.9
      - @types/http-errors@2.0.3
      - @types/jsonwebtoken@9.0.4
      - @types/morgan@1.9.7
      - @types/swagger-ui-express@4.1.5
      - @types/uuid@9.0.6

## 1.0.2

### Patch Changes

- Swagger UI
  - Show only one version of the APi and reduce duplication [69181ca79e0d39e4749f10f4de200fd1ec67dcd8]
  - Only shows servers referent to the URL it it has been accessed [1ed12c68d41fc940507d5b890449eed5ce763f2f]
- Update dependencies [3bd7f9b64a561ef2259ce833c5c51ac76cacac39]:
  - core:
    - update:
      - @ts-rest/express@3.27.0
      - @ts-rest/open-api@3.27.0

## 1.0.1

### Patch Changes

Rename endpoints functions.

- v1.providers.providers => v1.providers.getAll
- v1.users.linkedAccounts => v1.users.getLinkedAccounts
- v1.users.linkAccountUrl => v1.users.getLinkAccountUrl

**Attention**: This is not a breaking change for the API. But it **breaks the adapter**. However, since we are not publicly launched yet, this is a **patch change**.

- Updated dependencies
- @lincs.project/auth-api-contract@1.0.1
