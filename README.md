# LINCS Auth API

This is a monorepo for the LINCS Auth API service.

## Auth API Service

This service exposes Keycloak functionality to authorized LINCS applications. We use [Express](https://expressjs.com/) and [TS-REST](https://ts-rest.com/) as the backbone to build the server, combined with [@keycloak/keycloak-admin-client](https://www.npmjs.com/package/@keycloak/keycloak-admin-client) and
[keycloak-connect](https://github.com/keycloak/keycloak-nodejs-connect) to access, control, and protect the Keycloak instance. The endpoints require an authorization token with access to the _admin_ role of the _admin-cli_ client in Keycloak. This token can be associated with a service account or a regular user account.

[See more here](./apps/api/)

## Auth API Contract

This library defines the contract for the [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api). It uses [TS-REST](https://ts-rest.com/) in combination with [Zod](https://zod.dev/) to determine the request and response objects. These objects can be used to validate calls to each endpoint.

It can be used to build a JavaScript helper to access the endpoints of the [Auth-API Service App](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/apps/api) using a standardized fetcher. It allows for better endpoint discoverability and fully typed request parameters and responses. The API contract is also available to create a custom fetcher.

[See more here](./packages/lincs.project-api-auth-api-contract/)

## Development

We use [Turborepo](https://turbo.build/) to manage this monorepo. It includes the following **apps**, **packages**, and **utilities**:

### Apps

- `api`: Auth Api Service to access Lincs Project Keycloak functionalities

### Packages

- `@lincs.project/api-auth-api-contract`: Auth Api Contract

### Utility Packages

- `tsconfig` for static type checking with [TypeScript](https://www.typescriptlang.org/)
- `eslint-config-custom` for code linting [ESLint](https://eslint.org/) for code linting

### Versioning

Since this is a contract for a web service API, it is important to strategically think about changes as they might break other services that depend on this API. We started with version `v1`. See [auth-api-contract](https://gitlab.com/calincs/infrastructure/auth-api/-/tree/main/packages/lincs.project-auth-api-contract).

A further development that adds, removes, or changes the endpoints should be allocated in a new version. However, keep the previous version available to allow other services to upgrade their codebase.

### Linter

**Attention!**

This project uses and deploys ESM. We have configured a tight linter with strict Typescript and format (prettier) rules. Some of these rules can be relaxed if needed. Check eslint config: `.eslintrc.cjs`.

The linter always runs before any commit. But you can also run it at any time to check the code.

```node
npm run lint
```

### Run with Docker

```bash
docker compose up
```

This should make the service accessible at `http://localhost:3001`

### Run locally with Node

If you want to host the API locally without using Docker, you must have Node.js and npm installed (instructions for how to download these dependencies can be found [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)).

To see if you already have Node.js and npm installed, run the following commands:

```bash
node -v
npm -v
```

Then follow these steps:

1. Clone this repository on your machine.
2. Install the dependencies

```node
npm install
```

3. Start the API by typing:

```node
npm run dev
```
